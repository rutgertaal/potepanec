require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe 'product/show' do
    let!(:taxon) { create(:taxon) }
    let!(:product) { create(:product, taxons: [taxon]) }
    let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }

    before do
      get :show, params: { id: product.id }
    end

    it 'returns response successful' do
      expect(response).to be_successful
    end
    it 'returns show template' do
      expect(response).to render_template :show
    end
    it 'assigns target product' do
      expect(assigns(:product)).to eq product
    end
    it 'returns maximum allowed related products' do
      expect(assigns(:related_products).count).to eq 4
    end
  end
end

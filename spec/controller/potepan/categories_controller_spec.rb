require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  describe 'category/show' do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon, taxonomy: taxonomy) }
    let(:products) { create(:product, taxons: [taxon]) }

    before do
      get :show, params: { id: taxon.id }
    end

    it 'returns response successful' do
      expect(response).to be_successful
    end
    it 'returns show template' do
      expect(response).to render_template :show
    end
    it 'displays the taxonomy' do
      expect(assigns(:taxonomies)).to match_array(taxonomy)
    end
    it 'displays the taxon' do
      expect(assigns(:taxon)).to eq taxon
    end
    it 'displays the product' do
      expect(assigns(:products)).to match_array(products)
    end
  end
end

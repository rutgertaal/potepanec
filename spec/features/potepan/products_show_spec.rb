require "rails_helper"

RSpec.feature 'Show Page', type: :feature do
  let!(:taxon) { create(:taxon) }
  let!(:product) { create(:product, taxons: [taxon]) }
  let!(:related_products) { create_list(:product, 4, taxons: [taxon]) }

  before do
    visit potepan_product_path(id: product.id)
  end

  scenario 'User opens single product page' do
    expect(page).to have_title("#{product.name} | PotepanEC")
    expect(page).to have_content(product.name)
    expect(page).to have_content(product.display_price)
    expect(page).to have_content(product.description)
    expect(page).to have_content(related_products.first.display_price)
    expect(page).to have_content(related_products.second.name)
  end

  scenario 'User clicks link to go back to current product category list' do
    click_on '一覧ページへ戻る'
    expect(page).to have_current_path potepan_category_path(product.taxons.first.id)
  end
end

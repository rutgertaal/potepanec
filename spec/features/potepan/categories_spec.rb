require "rails_helper"

RSpec.feature 'Categories', type: :feature do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, name: 'taxon_1', taxonomy: taxonomy, parent: taxonomy.root) }
  let!(:product_1) { create(:product, name: 'product_1', taxons: [taxon]) }
  let!(:product_2) { create(:product, name: 'product_2', price: 20) }

  before do
    visit potepan_category_path(taxon.id)
  end

  scenario 'Category contains correct products' do
    expect(taxon.products).to match_array([product_1])
    expect(taxon.products).not_to match_array([product_2])
  end

  scenario 'User opens category page' do
    expect(page).to have_title("#{taxon.name} | PotepanEC")
    expect(page).to have_link product_1.name, href: potepan_product_path(product_1.id)
    expect(page).not_to have_link product_2.name, href: potepan_product_path(product_2.id)

    within '.productBox' do
      expect(page).to have_content(product_1.name)
      expect(page).to have_content(product_1.display_price)
      expect(page).not_to have_content(product_2.name)
      expect(page).not_to have_content(product_2.display_price)
    end
  end

  scenario 'User clicks on product' do
    click_link product_1.name
    expect(page).to have_current_path(potepan_product_path(product_1.id))
  end
end

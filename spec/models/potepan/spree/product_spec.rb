require 'rails_helper'

RSpec.describe Spree::Product, type: :model do
  let(:option_type_size) { create(:option_type, presentation: 'Size') }
  let(:available_sizes) do
    [
      create(:option_value, presentation: 'S', option_type: option_type_size),
      create(:option_value, presentation: 'M', option_type: option_type_size),
      create(:option_value, presentation: 'L', option_type: option_type_size),
      create(:option_value, presentation: 'XL', option_type: option_type_size)
    ]
  end
  let!(:variant) { create(:variant, product: product, option_values: available_sizes) }
  let!(:taxon) { create(:taxon) }
  let!(:product) { create(:product, taxons: [taxon], option_types: [option_type_size]) }
  let!(:related_product) { create(:product, taxons: [taxon]) }

  describe '.product_options' do
    it 'gets all available product sizes' do
      expect(product.product_options('Size').map(&:presentation)).to eq %w[S M L XL]
    end
  end

  describe '#related_products' do
    it 'matches related product' do
      expect(product.related_products).to match_array(related_product)
    end
  end
end

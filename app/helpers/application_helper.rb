module ApplicationHelper
  # Page title
  def full_title(page_title = '')
    base_title = 'PotepanEC'
    page_title.blank? ? base_title : "#{page_title} | #{base_title}"
  end
end

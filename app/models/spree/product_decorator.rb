Spree::Product.class_eval do
  MAX_RELATED_PRODUCTS = 4

  def product_options(value)
    option_types.find_by(presentation: value)&.option_values
  end

  def related_products
    Spree::Product.in_taxons(taxons).where.not(id: id).distinct.limit(MAX_RELATED_PRODUCTS)
  end
end
